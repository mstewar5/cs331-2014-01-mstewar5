Lab report for Deque Lab - mstewar5
Timestamp: 2014-04-21 12:21:19

|  :scenario |                                :title |                              :result | :score |
|------------+---------------------------------------+--------------------------------------+--------|
|    working | Test student code vs instructor tests |                                 Pass |     45 |
| qualifying | Test student tests vs instructor code |                                 Pass |      5 |
|    broke-1 |             deque-size only returns 0 | Pass: student tests detected errors. |      5 |
|    broke-2 |     push-front doesn't increment size | Pass: student tests detected errors. |      5 |
|    broke-3 |        pop-back lets size go negative | Pass: student tests detected errors. |      5 |
|    broke-4 |            pop-front doesn't dec size | Pass: student tests detected errors. |      5 |
|    broke-5 |          push-back inserts into front | Pass: student tests detected errors. |      5 |
|    broke-6 |     flip-front doesn't clear the back | Pass: student tests detected errors. |      5 |
|    broke-7 |     flip-back doesn't clear the front | Pass: student tests detected errors. |      5 |
|    broke-8 |                back doesn't flip list | Pass: student tests detected errors. |      5 |
|    broke-9 |        front references the back list | Pass: student tests detected errors. |      5 |
|   broke-10 |               flip-front flips always | Pass: student tests detected errors. |      5 |
|      total |                           Total Score |                                      |    100 |

Details:


** Details for Test student code vs instructor tests

All checks (100) succeeded.


** Details for Test student tests vs instructor code

All checks (10) succeeded.


** Details for deque-size only returns 0


FAIL "about back - Gets back of our custom Deque." at (t_core.clj:50)
    Expected: 2
      Actual: 3

FAIL "about pop-front - Pops first element from front of our custom Deque." at (t_core.clj:58)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front (3), :size 1}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front (4 3), :size 2}::deque.core.Deque

FAIL "about pop-back - Pops first element from back of our custom Deque." at (t_core.clj:66)
    Expected: {:back (4), :front clojure.lang.PersistentList$EmptyList@1, :size 1}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front (4 3), :size 2}::deque.core.Deque

FAIL "about pop-back - Pops first element from back of our custom Deque." at (t_core.clj:67)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front clojure.lang.PersistentList$EmptyList@1, :size 0}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front (4 3), :size 2}::deque.core.Deque
FAILURE: 4 checks failed.  (But 6 succeeded.)
Subprocess failed


** Details for push-front doesn't increment size


FAIL "about push-front - Pushes elt to the front of our custom Deque." at (t_core.clj:9)
    Expected: {:back (3), :front (4 1 2), :size 4}::deque.core.Deque
      Actual: {:back (3), :front (4 1 2), :size 3}::deque.core.Deque
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for pop-back lets size go negative


FAIL "about pop-back - Pops first element from back of our custom Deque." at (t_core.clj:67)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front clojure.lang.PersistentList$EmptyList@1, :size 0}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front clojure.lang.PersistentList$EmptyList@1, :size -1}::deque.core.Deque
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for pop-front doesn't dec size


FAIL "about pop-front - Pops first element from front of our custom Deque." at (t_core.clj:58)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front (3), :size 1}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front (3), :size 2}::deque.core.Deque
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for push-back inserts into front


FAIL "about push-back - Pushes elt to the back of our custom Deque." at (t_core.clj:17)
    Expected: {:back (4 3), :front (1 2), :size 4}::deque.core.Deque
      Actual: {:back (3), :front (4 1 2), :size 4}::deque.core.Deque
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for flip-front doesn't clear the back


FAIL "about flip-front - Flips back to front of our custom Deque." at (t_core.clj:25)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front (4 3), :size 2}::deque.core.Deque
      Actual: {:back (3 4), :front (4 3), :size 2}::deque.core.Deque
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for flip-back doesn't clear the front


FAIL "about flip-back - Flips front to back of our custom Deque." at (t_core.clj:33)
    Expected: {:back (3 4), :front clojure.lang.PersistentList$EmptyList@1, :size 2}::deque.core.Deque
      Actual: {:back (3 4), :front (4 3), :size 2}::deque.core.Deque

FAIL "about pop-back - Pops first element from back of our custom Deque." at (t_core.clj:66)
    Expected: {:back (4), :front clojure.lang.PersistentList$EmptyList@1, :size 1}::deque.core.Deque
      Actual: {:back (4), :front (4 3), :size 1}::deque.core.Deque

FAIL "about pop-back - Pops first element from back of our custom Deque." at (t_core.clj:67)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front clojure.lang.PersistentList$EmptyList@1, :size 0}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front (4 3), :size 0}::deque.core.Deque
FAILURE: 3 checks failed.  (But 7 succeeded.)
Subprocess failed


** Details for back doesn't flip list


FAIL "about back - Gets back of our custom Deque." at (t_core.clj:50)
    Expected: 2
      Actual: nil
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for front references the back list


FAIL "about front - Gets front of our custom Deque." at (t_core.clj:41)
    Expected: 1
      Actual: 3
FAILURE: 1 check failed.  (But 9 succeeded.)
Subprocess failed


** Details for flip-front flips always


FAIL "about front - Gets front of our custom Deque." at (t_core.clj:41)
    Expected: 1
      Actual: 3

FAIL "about pop-front - Pops first element from front of our custom Deque." at (t_core.clj:58)
    Expected: {:back clojure.lang.PersistentList$EmptyList@1, :front (3), :size 1}::deque.core.Deque
      Actual: {:back clojure.lang.PersistentList$EmptyList@1, :front clojure.lang.PersistentList$EmptyList@1, :size 1}::deque.core.Deque
FAILURE: 2 checks failed.  (But 8 succeeded.)
Subprocess failed
